<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <h5>List all Prime Numbers</h5>
      Enter Max Range : <input id="range" type="number">
      <button onclick="compute()">Submit</button>
    <p id="result"></p>
  </body>
  <script type="text/javascript">
    function compute () {
      var range = parseInt(document.getElementById("range").value);
      var primes = [2],isPrime;

      for (var i=3;i<=range;i+=2){
          isPrime = true;
          for (var j = 0;j<primes.length;j++){
              if (i%primes[j]==0){
                  isPrime=false;
                  break;
              }
          }
          if(isPrime){primes.push(i)}
      }
       document.getElementById("result").innerHTML = JSON.stringify(primes);
    }
  </script>
</html>
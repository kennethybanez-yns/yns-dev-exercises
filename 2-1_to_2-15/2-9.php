<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <button id="red" onClick="changeColor(this.id)">red</button><br>
    <button id="blue" onClick="changeColor(this.id)">blue</button><br>
    <button id="green" onClick="changeColor(this.id)">green</button><br>
  </body>
  <script type="text/javascript">
    function changeColor(color) {
      document.body.style.backgroundColor = color;
    }
  </script>
</html>
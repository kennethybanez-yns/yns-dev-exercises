<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
  </body>
  <script>
    var i = 240;
    var time = 300;
    function animateBg(i) {
      document.body.style.backgroundColor = 'hsl(' + i + ', 100%, 31%)';
      //  hsl(195, 53%, 79%)
      console.log(i);
    }
    setInterval(function() {
      animateBg(--i)
    }, time);
  </script>
</html>
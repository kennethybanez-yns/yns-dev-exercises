<?php 

  require 'config.php';

  $questions = [];
  $quizId = null;
  /**GET ALL QUESTIONS***/
  $query = "SELECT id, question FROM quiz_questions ORDER BY RAND() LIMIT 10";
  $statement = $connection->prepare($query);
  $statement->bind_result(
    $id,
    $question
  );
  $statement->execute();
  while($statement->fetch()) {
    $questions[] = [
      'id' => $id,
      'question' => $question,
      'choices' => []
    ];
  }
  $statement->close();
  /***GET ALL CHOICES PER QUESTION***/
  $query  = "SELECT id, quiz_question_id, choice, correct FROM quiz_choices WHERE quiz_question_id = ?";
  $statement = $connection->prepare($query);
  $statement->bind_param('i',$quizId);
  $statement->bind_result(
    $id,
    $quizQuestionId,
    $choice,
    $correct
  );
  for ($i=0; $i < sizeof($questions) ; $i++) { 
    $quizId = $questions[$i]['id'];
    $statement->execute();
    while($statement->fetch()) {
      $questions[$i]['choices'][] = [
        'id' => $id,
        'quizId' => $quizQuestionId,
        'choice' => $choice,
        'correct' =>$correct
      ];
    }
  }
  $statement->close();
  $connection->close();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <?php
      session_start();
      if (isset($_SESSION['username'])) {
          if (file_exists('info.csv')) {
              $csv= file_get_contents('info.csv');
              $array = array_map("str_getcsv", explode("\n", $csv));
              $jsonConvertedCsv = json_encode($array);
              $jsonConvertedCsv = json_decode($jsonConvertedCsv, true);
              $paginatedCsv = [];
              if (sizeof($jsonConvertedCsv) > 0) {
                  $page = isset($_GET['paging']) ? intval($_GET['paging'])  : 1;
                  $limit = 10; 
                  $offset = (($page - 1) * $limit) + 1; 
                  $totalItems = count($jsonConvertedCsv) - 2;
                  $totalPages = ceil($totalItems / $limit) ;
                  array_push($paginatedCsv, $jsonConvertedCsv[0]);
                  for ($i=$offset; $i < ($limit + $offset) ; $i++) { 
                    if (empty($jsonConvertedCsv[$i]))  {
                    } else {
                        array_push($paginatedCsv,$jsonConvertedCsv[$i]);
                    }
                  }
              }
          } else {
              echo("No info submitted");
          }
      } else {
          header("Location: index.php");
      }  
    ?>
    <table border="1">
      <thead>
        <tr>
          <?php foreach ($paginatedCsv[0] as $header): ?>
            <th><?php echo $header; ?></th>
          <?php endforeach;  array_shift($paginatedCsv);?>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($paginatedCsv as $key => $value): ?>
          <tr>
            <?php  foreach ($paginatedCsv[$key] as $index => $info): ?>
              <?php if ($index === 5) {?>
                <td><img src="<?php echo 'pictures/'. $info ?>" width="60px;" height="60px;"></td>
              <?php } else {?>  
                <td><?php echo $info; ?></td>
              <?php }?>
            <?php endforeach; ?>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <p>Showing page <?php echo $page. " of " .$totalPages; ?></p>
    <a href="?paging=<?php  echo ($page-1 === 0 ? 1 : $page-1);  ?>">Previous</a>
    ..........
    <a href="?paging=<?php echo  ($page+1 > $totalPages ? $page : $page+1); ?>">Next</a>
    <br>
    <form action="logout.php" method="POST">
      <input type="submit" value="Logout">
    </form>
  </body>
</html>
<?php

  session_start();

  function validator () {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
      return 1;
    }
    if (!isset(
      $_POST['uname'],
      $_POST['pword'])) {
        return 2;
    }
    if (!file_exists('info.csv')) {
        return 3;
    }
    return 0;
  }
  function clean_string($value) {
    $data = trim($value);
    $data = filter_var($data, FILTER_SANITIZE_STRING);
    $data = htmlspecialchars_decode($data, ENT_QUOTES);
    return $data;
  }

  if (validator() === 0) {
      $username = clean_string($_POST['uname']);
      $password = clean_string($_POST['pword']);
      $password = md5($password);
      $csv= file_get_contents('info.csv');
      $array = array_map("str_getcsv", explode("\n", $csv));
      $jsonConvertedCsv = json_encode($array);
      $jsonConvertedCsv = json_decode($jsonConvertedCsv, true);
      $totalItems = count($jsonConvertedCsv);
      $positiveUsername = 0;
      $positivePassword = 0;

      for ($i=0; $i < $totalItems-1; $i++) { 
        if ($username === $jsonConvertedCsv[$i][0]) {
            $positiveUsername = 1;
        }
        if ($password === $jsonConvertedCsv[$i][6]) {
            $positivePassword = 1;
        }
      }
      if ($positiveUsername === 1 && $positivePassword === 1) {
        $_SESSION['username'] = $username;
        echo "redirect";
        header("Location: list_page.php");
      } else {
        echo "Invalid credentials";
      }
  } else {
      if (validator() === 1) {
      } elseif (validator() === 2) {
          echo "incomplete fields";
      } elseif (validator() === 3) {
          echo "csv file is not yet generated";
      }
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <h4>LOGIN FORM</h4>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" enctype="multipart/form-data">
      Enter Firstname : <input name="uname" type="text" required><br>
      Enter Password : <input name="pword" type="password" required><br>
      <input type="submit">
    </form>
    <a href="signup.php">Signup</a><br>
    <a href="../">Back</a><br>
  </body>
</html>
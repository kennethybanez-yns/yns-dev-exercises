-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2020 at 10:23 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quizdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `quiz_choices`
--

CREATE TABLE `quiz_choices` (
  `id` int(11) NOT NULL,
  `quiz_question_id` int(11) NOT NULL,
  `choice` text DEFAULT NULL,
  `correct` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quiz_choices`
--

INSERT INTO `quiz_choices` (`id`, `quiz_question_id`, `choice`, `correct`) VALUES
(1, 1, 'earth', 1),
(2, 1, 'Mars', 0),
(3, 1, 'Venus', 0),
(4, 1, 'Jupiter', 0),
(5, 2, 'Tony Parker', 0),
(6, 2, 'John Parker', 0),
(7, 2, 'Ben Parker', 1),
(8, 2, 'Ironman', 0),
(11, 3, 'Galactus', 0),
(12, 3, 'Thanos', 1),
(13, 3, 'Joker', 0),
(14, 3, 'Ultron', 0),
(15, 4, 'Captain America', 0),
(16, 4, 'Spiderman', 0),
(17, 4, 'Hawkeye', 0),
(18, 4, 'Thor', 1),
(19, 5, 'Peggy Carter', 1),
(20, 5, 'Natasha Romanof', 0),
(21, 5, 'Segunda Katigbak', 0),
(22, 5, 'Captain Marvel', 0),
(23, 6, 'Kate', 0),
(24, 6, 'Morgan', 1),
(25, 6, 'Mary', 0),
(26, 6, 'Ana', 0),
(27, 7, 'Vibranium Shield', 0),
(28, 7, 'infinity Gaunlet', 1),
(29, 7, 'Ironman suit', 0),
(30, 7, 'Mjolnir', 0),
(31, 8, 'Doctor Strange', 1),
(32, 8, 'Thanos', 0),
(33, 8, 'Black Panther', 0),
(34, 8, 'Captain Marvel', 0),
(35, 9, 'Asgard', 0),
(36, 9, 'Quantum Realm', 1),
(39, 9, 'New York City', 0),
(40, 9, 'Volmir', 0),
(41, 10, 'I am Groot', 0),
(42, 10, 'We are Venom', 0),
(43, 10, 'I am legend', 0),
(44, 10, 'I am Ironman', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_questions`
--

CREATE TABLE `quiz_questions` (
  `id` int(11) NOT NULL,
  `question` text DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quiz_questions`
--

INSERT INTO `quiz_questions` (`id`, `question`, `date_created`) VALUES
(1, 'What is the name of our planet?', '2020-03-17 08:54:24'),
(2, 'Who is Peter Parker\'s uncle?', '2020-03-17 08:54:24'),
(3, 'Who wanted to eliminate half of the universe?', '2020-03-17 08:54:24'),
(4, 'Who is the owner of the battleaxe \'stormbreaker\'?', '2020-03-17 08:54:24'),
(5, 'What is the name of Captain America\'s love interest?', '2020-03-17 08:54:24'),
(6, 'What is the name of Tony Stark\'s daughter?', '2020-03-17 08:54:24'),
(7, 'It is used to hold the power of the six infinity stones?', '2020-03-17 08:54:24'),
(8, 'Who is the guardian of the time stone?', '2020-03-17 08:54:24'),
(9, 'What is the name of the place where Ant-Man was stucked for 5 hrs?', '2020-03-17 08:54:24'),
(10, 'What was the last words that Ironman said?', '2020-03-17 08:54:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `quiz_choices`
--
ALTER TABLE `quiz_choices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quiz_question_id` (`quiz_question_id`);

--
-- Indexes for table `quiz_questions`
--
ALTER TABLE `quiz_questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quiz_choices`
--
ALTER TABLE `quiz_choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `quiz_questions`
--
ALTER TABLE `quiz_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `quiz_choices`
--
ALTER TABLE `quiz_choices`
  ADD CONSTRAINT `quiz_choices_ibfk_1` FOREIGN KEY (`quiz_question_id`) REFERENCES `quiz_questions` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

  header('Access-Control-Allow-Methods: POST');

  /******************************************/
  /********** VERIFICATION FUNCTIONS **********/
  /******************************************/

  function validator () {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
      return 1;
    }
    if (!isset(
      $_POST['fname'],
      $_POST['lname'],
      $_POST['mname'],
      $_POST['email'],
      $_POST['pword'],
      $_POST['bday']))
    {
        return 2;
    }
    if (empty($_FILES['picture'])) {
        return 3;
    }
    return 0;
  }

  function clean_string($value) {
    $data = trim($value);
    $data = filter_var($data, FILTER_SANITIZE_STRING);
    $data = htmlspecialchars_decode($data, ENT_QUOTES);
    return $data;
  }

  /****************************************************/
  $success = 0;
  if (validator() === 0) {
      // declare variables
      $fname = clean_string($_POST['fname']);
      $mname = clean_string($_POST['mname']);
      $lname = clean_string($_POST['lname']);
      $email = $_POST['email'];
      $pword = md5($_POST['pword']);
      $bday = date_create($_POST['bday']);
      $bday = date_format($bday,"M d, Y");
      $existingRecord = [];
      $fileName = "info.csv"; 
      
      /****************************************************/
      // photo related variables
      $targetDir = "pictures/";
      $targetFile = $targetDir . basename($_FILES["picture"]["name"]);    
      $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
      $imageNewName = 'pic-'. $fname .'-'. $lname .'.'. $imageFileType;
      $targetFile = $targetDir.$imageNewName;
      $tmpName = $_FILES["picture"]["tmp_name"];
      $check = getimagesize($_FILES["picture"]["tmp_name"]);

      if ($check === false) {
          echo 'Image you uploaded is not a real image';
      }
      if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
          echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
      }
      if (!move_uploaded_file($tmpName, $targetFile)) {
          echo 'Photo was not uploaded';
      } 
      /****************************************************/
      // check if a csv file was already created
      if (file_exists($fileName)) {
        if (($handle = fopen($fileName, "r")) !== FALSE) { 
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {  
              $existingRecord[] = $data;
              unset($existingRecord[0]);
            }  
          fclose($handle);  
        }  
      }
      // push variable inside array
      $existingRecord[] = [
        $fname,
        $mname,
        $lname,
        $email,
        $bday,
        $imageNewName,
        $pword
      ];
      /****************************************************/
      $output = fopen("info.csv", "w");  
      fputcsv($output, array(
        'First_Name',
        'Middle_Name',
        'Last_Name',
        'Email',
        'Birthdate',
        'Image',
        'Password'
      ));  
      foreach ($existingRecord as $line) {
        fputcsv($output, $line);
      }
      fclose($output);  
      /****************************************************/
      $success = 1;
  } else {
      if (validator() === 1) {
          echo "Wrong Request Type";
      } elseif (validator() === 2) {
          echo "Incomplete Fields";
      } elseif (validator() === 3) {
          echo "Picture Needed";
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
  <body>
    <?php
      if ($success === 1) {
    ?>
    <p><?php  echo 'SUCCESSFULLY SUBMITTED';?></p><br>
    <p><?php  echo 'Fullname : '. $fname .' '. $mname .' '. $lname;?></p><br>
    <p><?php  echo 'Email : '. $email;?></p><br>
    <p><?php  echo 'Birthdate : '. $bday;?></p><br>
    <?php }?>
    <a href="index.php">Login now!</a>
  </body>
</html>
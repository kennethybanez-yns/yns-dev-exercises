<?php

  //MAIN VARIABLES
  $serverName = 'localhost';
  $dbUsername = 'root';
  $dbPassword = '';
  $dbName = 'quizDb';
 // error_reporting(0);
  
  $connection = new mysqli($serverName, $dbUsername, $dbPassword, $dbName);

  if ($connection->connect_error) {
      echo('Failed to connect!');
  } else {
      mysqli_set_charset($connection, 'utf8');
      date_default_timezone_set('Asia/Manila');
      $connection->autocommit(FALSE);
  }
?>


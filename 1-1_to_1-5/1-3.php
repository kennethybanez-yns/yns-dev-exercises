<?php

  function gcd($num1, $num2) { 
    if ($num1 == 0) {
        return $num2; 
    }
    if ($num2 == 0) {
        return $num1; 
    }
    if ($num1 == $num2) {
        return $num1 ; 
    }
    if ($num1 > $num2) {
        return gcd( $num1-$num2 , $num2 ) ; 
    }
    return gcd( $num1 , $num2-$num1 ) ; 
  } 

  $num1 = 0;
  $num2 = 0;
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['num1'],$_POST['num2'])) {
        $num1 = $_POST['num1'];
        $num2 = $_POST['num2'];
    } else {
        echo "Please enter the numbers";
    }
  }  
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h5>Show the greatest common divisor.</h5>
  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    Enter Num1 : <input name="num1" type="number" required><br>
    Enter Num2 : <input name="num2" type="number" required><br>
    <input type="submit">
  </form>
  <p><?php echo "GCD of $num1 and $num2 is ", gcd($num1,$num2);?></p>
</body>
</html>
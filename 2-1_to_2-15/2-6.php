<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <div>
      <button onclick="addLabel()">Add Label</button>
      <label id="display" for=""></label>
    </div>
  </body>
  <script>
    function addLabel() {
      var lbl = document.createElement("LABEL");
      lbl.innerHTML = "New Label";
      document.body.appendChild(lbl);
    }
  </script>
</html>
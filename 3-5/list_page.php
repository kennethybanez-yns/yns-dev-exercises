<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <?php

      session_start();

      require 'api/config.php';
      require 'api/cleaners.php';

     // checkRequestType('POST');
      checkToken();
      
      if (fetchValidator() === 0) {
          $page =  $_GET['page'];
          $size =  $_GET['size'];
          $userList = [];
          $totalPages = 0;
          $offset = ($page - 1) * $size;
          $perPageQuery = "SELECT id, first_name, middle_name, last_name, email, birthday, image, password FROM users LIMIT ?, ?";
          $totalPageQuery = "SELECT COUNT(*) FROM users";
          
          $statement = $connection->prepare($perPageQuery);
          $statement->bind_param('ii',
            $offset,
            $size
          );
          $statement->bind_result(
            $id,
            $fname,
            $mname,
            $lname,
            $email,
            $bday,
            $image,
            $pword
          );
          $statement->execute();
          while ($statement->fetch()) {
            $userList[] = [
              'id' => $id,
              'fname' => $fname,
              'mname' => $mname,
              'lname' => $lname,
              'bday' => $bday,
              'image' => $image,
              'pword' => $pword
            ];
          }
          $statement->close();

          $statement = $connection->prepare($totalPageQuery);
          $statement->bind_result(
            $totalCount
          );
          $statement->execute();
          $statement->fetch();
          $connection->close();
          $totalPages = ceil($totalCount / $size); 
      } else {
          if (fetchValidator() === 2) {
              echo "incomplete fields";
          } 
      }
    ?>
    <table border="1">
      <thead>
        <tr>
          <th>FIRSTNAME</th>
          <th>MIDDLENAME</th>
          <th>LASTNAME</th>
          <th>BIRTHDAY</th>
          <th>EMAIL</th>
          <th>PASSWORD</th>
          <th>IMAGE</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <?php foreach ($userList as $key => $value): ?>
            <tr>
              <?php  foreach ($userList[$key] as $index => $info): ?>
                <?php if ($index === 'image') {?>
                  <td><img src="<?php echo  $info; ?>" width="60px;" height="60px;"></td>
                <?php } else {?>  
                  <td><?php echo $info; ?></td>
                <?php }?>
              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tr>
      </tbody>
    </table>
    <p>Showing page <?php echo $page. " of " .$totalPages; ?></p>
    <br>
    <a href="?size=10&page=<?php  echo  ($page-1 === 0 ? 1 : $page-1);  ?>">Previous</a>
    ..........
    <a href="?size=10&page=<?php echo  ($page+1 > $totalPages ? $page : $page+1); ?>">Next</a>
    <br>
    <form action="logout.php" method="POST">
      <input type="submit" value="Logout">
    </form>
  </body>
</html>
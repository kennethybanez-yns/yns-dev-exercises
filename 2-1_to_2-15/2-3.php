<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <h5>Four Math Basic Operations</h5>
    Number 1 : <input id="num1" type="number"><br>
    Number 2 : <input id="num2" type="number"><br>
    <select name="" id="operation">
      <option value="plus">Addition</option>
      <option value="minus">Subtraction</option>
      <option value="times">Multiplication</option>
      <option value="divide">Division</option>
    </select><br>
    <button onclick="compute()" >Submit</button>
    <p id="result1"></p>
    <p id="result2"></p>
  </body>
  <script type="text/javascript">
    function compute() {
      var num1 = parseInt(document.getElementById("num1").value);
      var num2 = parseInt(document.getElementById("num2").value);
      var e = document.getElementById("operation");
      var operation = e.options[e.selectedIndex].value;
      var answer = {
        ans1 : 0,
        ans2 : 0
      };
      var statement = {
        stmt1 : '',
        stmt2 : ''
      };

      if (operation === 'plus') {
          answer.ans1 = num1 + num2;
      } else if (operation === 'minus') {
          answer.ans1 = num1 - num2;
          answer.ans2 = num2 - num1; 
      } else if (operation === 'times') {
          answer.ans1 = num1 * num2;
      } else if (operation === 'divide') {
          answer.ans1 = num1 / num2;
          answer.ans2 = num2 / num1;
      }
      if (operation === 'minus' || operation === 'divide') {
          statement.stmt2 = num2+ ' '+operation+' ' +num1+ ' = '+ answer.ans2;
      }
      statement.stmt1 = num1+ ' '+operation+' ' +num2+ ' = '+ answer.ans1;
      document.getElementById("result1").innerHTML = statement.stmt1;
      document.getElementById("result2").innerHTML = statement.stmt2;
    }
  </script>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <img id="photo"  border="0" src="morty.png"  width="100" height="100"><br>
    <button onclick="changeSize(20)">Small</button>
    <button onclick="changeSize(50)">Medium</button>
    <button onclick="changeSize(100)">Large</button>
  </body>
  <script>
    function changeSize(x) {
      photo = document.getElementById('photo');
      photo.style.height =  x + "px";
      photo.style.width = x + "px";
    }
  </script>
</html>
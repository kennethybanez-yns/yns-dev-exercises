<?php

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['date_entered'])) {
        $dateEntered = date_create($_POST['date_entered']);
        $baseDateEntered = $_POST['date_entered'];

        date_add($dateEntered,date_interval_create_from_date_string("3 days"));
        
        $dayName = date_format($dateEntered,"D");
        $dateEntered = date_format($dateEntered,"M d, Y");
    } else {
        echo "Please enter the date";
    }
  }  
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
  <body>
    <h5>Input date. Then show 3 days from inputted date and its day of the week.</h5>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
      Enter Date : <input name="date_entered" type="date" required><br>
      <input type="submit">
    </form>
    <?php if (isset($baseDateEntered)) {?>
    <p><?php echo date_format(date_create($baseDateEntered),"M d, Y"). ' added by 3 days will be : ' . $dateEntered .' ('. $dayName .')';?></p>
    <?php }?>
  </body>
</html>
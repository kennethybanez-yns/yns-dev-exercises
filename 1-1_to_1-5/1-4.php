<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h5>FizzBuzz problem.</h5>
  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    Enter Num1 : <input name="num1" type="number" required><br>
    <input type="submit">
  </form>

  <?php

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      if (isset($_POST['num1'])) {
        $num1 = $_POST['num1'];
        for ($i=1; $i <= $num1; $i++) { 
          if (($i % 3) === 0 && ($i % 5) !== 0) {
              echo ' '. "Fizz" .' ';
          } elseif (($i % 5) === 0 && ($i % 3) !== 0) {
              echo ' '. "Buzz" .' ';
          } elseif (($i % 5) === 0 && ($i % 3) === 0) {
              echo ' '. "FizzBuzz" .' ';
          }
          else {
            echo ' '. $i .' ';
          }
        }
      } else {
          echo "Please enter the numbers";
      }
    }  
  ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <img id="photo"  border="0" src="morty.png"  width="100" height="100">
    <select onchange="changePic(this.value)" name="" id="imageSelection">
      <option value="morty.png">morty</option>
      <option value="penguin.jpg">penguin</option>
      <option value="rick.png">rick</option>
    </select>
  </body>
  <script>
    function changePic (name) {
      document.getElementById("photo").src = name;
    }
  </script>
</html>
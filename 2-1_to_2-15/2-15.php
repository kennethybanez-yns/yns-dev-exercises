<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body onload="startTime()">
    <div id="txt"></div>
  </body>
  <script>
    function startTime() {
      var today = new Date();
      var datetoday = today.getDate();
      var monthNow = today.getMonth();
      var yearNow = today.getFullYear();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();
      const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
      ];
      m = checkTime(m);
      s = checkTime(s);
      document.getElementById('txt').innerHTML =
      monthNames[monthNow]  + ' ' + datetoday +', ' + yearNow + ' - ' + h + ":" + m + ":" + s;
      var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
      if (i < 10) {i = "0" + i};  
      return i;
    }
  </script>
</html>
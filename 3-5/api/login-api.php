<?php

  session_start();

  header('Access-Control-Allow-Methods: POST');

  require 'config.php';
  require 'cleaners.php';

  checkRequestType('POST');
  
  if (loginValidator() === 0) {
      $username = cleanString($_POST['uname']);
      $password = cleanString($_POST['pword']);
      $password = md5($password);
      // CHECKS IF THE ACCOUNT IS EXISTING
      $command = 'SELECT id FROM users WHERE BINARY first_name = ? AND BINARY password = ?';
      $statement = $connection->prepare($command);
      $statement->bind_param('ss', $username, $password);
      $statement->bind_result(
        $id
      );
      $statement->execute();
      $statement->store_result();
      $total_count = $statement->num_rows;
      $statement->fetch();  
      if ($total_count === 1) {
          $_SESSION['id'] = $id;
          echo "redirect";
          header("Location: ../list_page.php?page=1&size=10");
      } else {
          echo "Invalid credentials";
      }   
  } else {
      if (loginValidator() === 1) {
      } elseif (loginValidator() === 2) {
          echo "incomplete fields";
      } 
  }
?>
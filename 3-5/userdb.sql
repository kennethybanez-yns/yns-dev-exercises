-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2020 at 02:05 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `userdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `password` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `birthday`, `image`, `password`) VALUES
(1, 'kenneth', 'pogi', 'ybanez', 'asd@asd', 'Feb 21, 1990', 'pictures/pic-kenneth-ybanez.jpg', '81dc9bdb52d04dc20036dbd8313ed055'),
(2, 'luffy', 'd', 'monkey', '2@2', 'Feb 22, 1091', 'pictures/pic-2-2.jpg', 'c81e728d9d4c2f636f067f89cc14862c'),
(3, 'harry', 'half', 'OSBORNE', '3@3', 'Mar 31, 2019', 'pictures/pic-3-3.jpg', '6eca4ff77900c88431393e1748a32246'),
(4, 'peter', 'spidey', 'parker', '4@4', 'Apr 04, 0444', 'pictures/pic-4-4.jpg', 'a87ff679a2f3e71d9181a67b7542122c'),
(5, 'jollibee', 'bida', 'delivery', 'asd@asd.com', 'Mar 12, 1999', 'pictures/pic-jollibee-delivery.jpg', '310dcbbf4cce62f762a2aaa148d556bd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Document</title>
	</head>
	<body>
		<script type="text/javascript">
			var monthShifter = null;
			var dt= new Date();
			var month=dt.getMonth(); // read the current month
			function changeMonth(shift) {
				 if (shift === 'add') {
					 	if (month < 11) {
							month+=1;
						}
						dt.setMonth(month);
				} else if (shift === 'minus') {
						if (month > 0) {
							month-=1;
						}
						dt.setMonth(month)
				}
				var year=dt.getFullYear(); // read the current year
				dt=new Date(year, month, 01);//Year , month,date format
				var first_day=dt.getDay(); //, first day of present month
				dt.setMonth(month+1,0); // Set to next month and one day backward.
				var last_date=dt.getDate(); // Last date of present month
				var dy=1; // day variable for adjustment of starting date.
				const monthNames = ["January", "February", "March", "April", "May", "June",
					"July", "August", "September", "October", "November", "December"
				];
				document.write('<button onclick="changeMonth('+"'minus'"+')">Previous Month</button>');
				document.write('<label id="monthName" ></label>');
				document.write('<button onclick="changeMonth('+"'add'"+')">Next Month</button>');
				document.write ("<table><tr><td>Su</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td>");
				for (i=0;i<=41;i++) {
					if((i%7)==0) {
							document.write("</tr><tr>");
					} // if week is over then start a new line
					if ((i>= first_day) && (dy<= last_date)) {
							document.write("<td>"+ dy +"</td>");
							dy=dy+1;
					} else {
							document.write("<td>*</td>");
					} // Blank dates.
				} // end of for loop
				document.write("</tr></table>");
				document.close();
				document.getElementById('monthName').innerHTML = monthNames[month]; 
			}
			changeMonth('');
		</script>
	</body>
</html>
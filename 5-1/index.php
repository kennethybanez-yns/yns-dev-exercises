<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <button onclick="getScore()" style="font-size: 24px;">Check your score!</button>
    <label id="score" style="color:blue;font-size:18px;">0/10</label>
    <button onclick="retry()" style="font-size: 24px;">Retry Exam!</button>
    <table border='1'>
      <thead>
        <tr>
          <td>Question</td>
          <td>Choices</td>
        </tr>
      </thead>
      <tbody id="tableBody">
        <?php require 'api/get-questions.php';?>
        <?php for ($i=0; $i < sizeof($questions); $i++) { ?>
          <tr>
            <td><?php echo ($i+1). ') ' . $questions[$i]['question']; ?></td>
            <td> 
              <?php for ($a=0; $a <sizeof($questions[$i]['choices']); $a++) {  ?>
              <input type="radio"  name="<?php echo $i?>" value="<?php echo $questions[$i]['choices'][$a]['correct']?>">
              <label><?php echo $questions[$i]['choices'][$a]['choice']?></label><br>
              <?php }?>
            </td>
          </tr>
        <?php }?>  
      </tbody>
    </table>
    <script type="text/javascript">
      function getScore() {
        var score = 0;
        for (let index = 0; index < 10; index++) {
          var x = document.getElementsByName(index.toString());
          for (let i = 0; i < x.length; i++) {
            if (x[i].checked) {
              if (x[i].value == 1) {
                score++;
              }
            }
          }
        }
        console.log(score);
        document.getElementById('score').innerHTML = score + '/10'; 
      }  
      function retry() {
        location.reload();
      }
    </script>
  </body>
</html>
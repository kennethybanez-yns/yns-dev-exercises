<?php
  
  header('Access-Control-Allow-Methods: POST');

  require 'api/config.php';
  require 'api/cleaners.php';

  checkRequestType('POST');
  
  $success = 0;
  if (registrationValidator() === 0) {
      // declare variables
      $fname = cleanString($_POST['fname']);
      $mname = cleanString($_POST['mname']);
      $lname = cleanString($_POST['lname']);
      $email = $_POST['email'];
      $pword = md5($_POST['pword']);
      $bday = date_create($_POST['bday']);
      $bday = date_format($bday,"M d, Y");
      $existingRecord = [];
      $fileName = "info.csv"; 
      
      /****************************************************/
      // photo related variables
      $targetDir = "pictures/";
      $targetFile = $targetDir . basename($_FILES["picture"]["name"]);    
      $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
      $imageNewName = 'pic-'. $fname .'-'. $lname .'.'. $imageFileType;
      $targetFile = $targetDir.$imageNewName;
      $tmpName = $_FILES["picture"]["tmp_name"];
      $check = getimagesize($_FILES["picture"]["tmp_name"]);

      if ($check === false) {
          echo 'Image you uploaded is not a real image';
      }
      if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
          echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
      }
      if (!move_uploaded_file($tmpName, $targetFile)) {
          echo 'Photo was not uploaded';
      } else {
          $query = "INSERT INTO users(first_name, middle_name, last_name, email, birthday, image, password) VALUES (?, ?, ?, ?, ?, ?, ?)";
          $statement = $connection->prepare($query);
          $statement->bind_param('sssssss',
            $fname,
            $mname,
            $lname,
            $email,
            $bday,
            $targetFile,
            $pword
          );
          $statement->execute();
          if ($statement->affected_rows === 0) {
             echo "No record inserted";
          } else {
              $success = 1;
              $statement->close();
              $connection->commit();
              $connection->close();
          }
      }
  } else {
      if (registrationValidator() === 2) {
          echo "Incomplete Fields";
      } elseif (registrationValidator() === 3) {
          echo "Picture Needed";
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
  <body>
    <?php
      if ($success === 1) {
    ?>
    <p><?php  echo 'SUCCESSFULLY SUBMITTED';?></p><br>
    <p><?php  echo 'Fullname : '. $fname .' '. $mname .' '. $lname;?></p><br>
    <p><?php  echo 'Email : '. $email;?></p><br>
    <p><?php  echo 'Birthdate : '. $bday;?></p><br>
    <a href="index.php">Login now!</a>
    <?php }?>
  </body>
</html>
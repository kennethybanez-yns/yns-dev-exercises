<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
  <body>
    <h5>Input user information. Then show it in next page..</h5>
    <form action="second_page.php" method="POST" enctype="multipart/form-data">
      Enter Firstname : <input name="fname" type="text" required><br>
      Enter Middlename : <input name="mname" type="text" required><br>
      Enter Lastname : <input name="lname" type="text" required><br>
      Enter Password : <input name="pword" type="password" required><br>
      Enter Email : <input name="email" type="email" required><br>
      Enter Birthday : <input name="bday" type="date" required><br>
      Upload Profile Pic : <input name="picture" type="file" required><br>
      <input type="submit">
    </form>
  </body>
</html>
<?php
  // REMOVES UNNECESSARY CHARACTERS TO CLEAN THE STRING
  function cleanString($value) {
    // Removes leading and trailing spaces
    $data = trim($value);
    // Removes Unwanted Characters
    $data = filter_var($data, FILTER_SANITIZE_STRING);
    // Sanitizes HTML Characters
    $data = htmlspecialchars_decode($data, ENT_QUOTES);
    return $data;
  }
  // CLEANS A NUMBER
  function cleanNumber($value) {
    // Removes leading and trailing spaces
    $data = trim($value);
    // Removes Unwanted Characters
    $data = filter_var($data, FILTER_SANITIZE_NUMBER_INT);
    // Sanitizes HTML Characters
    $data = htmlspecialchars_decode($data, ENT_QUOTES);
    return $data;
  }
  function checkRequestType($type) {
    if ($_SERVER['REQUEST_METHOD'] !== $type) {
       header("Location: index.php");
    }
  }
  function registrationValidator () {
    if (!isset(
      $_POST['fname'],
      $_POST['lname'],
      $_POST['mname'],
      $_POST['email'],
      $_POST['pword'],
      $_POST['bday']))
    {
        return 2;
    }
    if (empty($_FILES['picture'])) {
        return 3;
    }
    return 0;
  }
  function loginValidator () {
    if (!isset(
      $_POST['uname'],
      $_POST['pword'])) {
        return 2;
    }
    return 0;
  }
  function fetchValidator () {
    if (!isset(
      $_GET['page'],
      $_GET['size'])) {
        return 2;
    }
    return 0;
  }
  function checkToken() {

    if (
      !isset(
        $_SESSION['id']
      )
    ) {
      header("Location: index.php");
    }
  }
?>